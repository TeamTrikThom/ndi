module.exports = function(grunt) {
  
    grunt.initConfig({
      concat: {
        js: {
          src: ['src/js/**/*.js', '!src/**/libs/**'],
          dest: 'dist/static/js/scripts.js',
        },
        jslibs: {
          src: ['src/js/libs/*.js'],
          dest: 'dist/static/js/libs/libs.js',
        },
        css: {
          src: ['src/css/**/*.css', '!src/css/**/*.ig.css'],
          dest: 'dist/static/css/styles.css',
        }
      },
      uglify: {
        js: {
          files: {
            'dist/static/js/scripts.min.js': ['src/js/**/*.js', '!src/**/libs/**']
          }
        },
        jslibs: {
          files: {
            'dist/static/js/libs/libs.min.js': ['src/js/libs/*.js']
          }
        },
      },
      cssmin: {
        options: {
          mergeIntoShorthands: false,
          roundingPrecision: -1
        },
        target: {
          files: {
            'dist/static/css/styles.min.css': ['dist/static/css/styles.css']
          }
        }
      },
      copy: {
        main: {
          files: [
            {
              expand: true, 
              cwd: 'src/', 
              src: ['.htaccess'], 
              dest: 'dist/'
            },
          ],
        },
        css: {
          files: [
            {
              expand: true, 
              cwd: 'src/css/', 
              src: ['**'], 
              dest: 'dist/static/css/'
            },
          ],
        },
      },
    });
  
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
  };