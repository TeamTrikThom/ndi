# INSTELLEN #

Voor de eerste keer om alles te installeren
```bash
npm i
```
Voor de website te maken in dist
```bash
npm run build
```
Voor de webite aan te passen en zelf te hosten
```bash
npm run dev
```