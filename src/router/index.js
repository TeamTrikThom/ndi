import Vue from 'vue';
import Router from 'vue-router';

import Home from '@/pages/Home.vue';
import Login from '@/pages/Login.vue';
import Notfound from '@/pages/404.vue';
import Duikclub from '@/pages/Duikclub.vue';
import Profile from '@/pages/Profile.vue';
import Reset from '@/pages/Reset.vue';
import Events from '@/pages/Events.vue';
import StartenMetDuiken from '@/pages/StartenMetDuiken.vue';

import Message from '@/components/Message.vue';
import Header from '@/components/Header.vue';
import Footer from '@/components/Footer.vue';
import Input from '@/components/Input.vue';
import DiveToSurface from '@/components/DiveToSurface.vue';
import LoginForm from '@/components/LoginForm.vue';
import Leden from '@/components/Leden.vue';
import Alerts from '@/components/Alerts.vue';
import Banner from '@/components/Banner.vue';
import Dialog from '@/components/Dialog.vue';
import Dashboard from '@/components/Dashboard.vue';
import Kalender from '@/components/Kalender.vue';
import Sponsor from '@/components/Sponsor.vue';


import Geschiedenis from '@/components/Duikclub/Geschiedenis.vue';
import Bestuur from '@/components/Duikclub/Bestuur.vue';
import Kader from '@/components/Duikclub/Kader.vue';
import Extra from '@/components/Duikclub/Extra.vue';

import Duikschool from '@/components/StartenMetDuiken/Duikschool.vue';
import LidWorden from '@/components/StartenMetDuiken/LidWorden.vue';
import Opleidingen from '@/components/StartenMetDuiken/Opleidingen.vue';
import Materiaal from '@/components/StartenMetDuiken/Materiaal.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {path: '', name:'home', component: Home , meta: { requireAuth: false }},
    {path: '/login', name:'login', component: Login, meta: { requireAuth: false, canAccessAuth: false }},
    {path: '/duikclub/:component', name:'duikclub', component: Duikclub, meta: { requireAuth: false }},
    {path: '/startenmetduiken/:component', name:'startenmetduiken', component: StartenMetDuiken, meta: { requireAuth: false }},
    {path: '/events', name:'events', component: Events, meta: { requireAuth: true }},
    {path: '/profiel/:user', name:'profiel', component: Profile, meta: { requireAuth: true }},
    {path: '/profiel/:user/:module', name:'profiel-module', component: Profile, meta: { requireAuth: true }},
    {path: '/reset', name:'reset', component: Reset, meta: { requireAuth: false }},
    {path: '/duikclub', component: Duikclub, meta: { requireAuth: false }},
    {path: '**', component: Notfound }
  ],
  mode: 'history'

});




Vue.component('trk-message', Message);
Vue.component('trk-header', Header);
Vue.component('trk-footer', Footer);
Vue.component('trk-input', Input);
Vue.component('trk-diveToSurface', DiveToSurface);
Vue.component('trk-login', LoginForm);
Vue.component('trk-leden', Leden);
Vue.component('trk-alerts', Alerts);
Vue.component('trk-geschiedenis', Geschiedenis);
Vue.component('trk-bestuur', Bestuur);
Vue.component('trk-kader', Kader);
Vue.component('trk-extra', Extra);
Vue.component('trk-banner', Banner);
Vue.component('trk-dialog', Dialog);
Vue.component('trk-dashboard', Dashboard);
Vue.component('trk-kalender', Kalender);
Vue.component('trk-sponsor', Sponsor);

Vue.component('trk-duikschool', Duikschool);
Vue.component('trk-lidworden', LidWorden);
Vue.component('trk-opleidingen', Opleidingen);
Vue.component('trk-materiaal', Materiaal);
