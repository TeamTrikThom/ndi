import Vue from 'vue';
import App from './App.vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';
import router from './router';
import Vuetify from 'vuetify'

import { store } from './store';
import FileUpload from 'v-file-upload';


Vue.use(FileUpload);
Vue.use(Vuetify);
Vue.use(VueResource);
Vue.use(Vuex);


new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  http: {
    emulateJSON: true,
    emulateHTTP: true,
    header: {
      Authorization: 'Basic Q45!Frs9'
    }
  },

});

router.beforeEach((to, from, next) => {

  if(to.meta.requireAuth) {
    if(store.state.authUserUUID == undefined) {
      store.commit({
        type: 'alert',
        message: 'Je moet zijn ingelogd voor deze actie te kunnen doen',
        alertType: 'error',
        value: true
      })
      store.dispatch('logout')
    }
  }
  if(to.meta.canAccessAuth == false) {
    if(store.state.authUserUUID != undefined) {
      router.push({name: 'home'})
    }
  }
  if(store.state.authUserUUID != undefined && store.state.authUserPass != undefined) {
    store.dispatch({
      type: 'check'
    });
  }
  next();
});
