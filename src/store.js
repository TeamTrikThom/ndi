import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';
import router from './router';


Vue.use(Vuex);
Vue.use(VueResource);


export const store = new Vuex.Store({
  state: {
    ip: 'https://rest.trikthom.nl/ndi',
    users: [],
    authProfilePhoto: 'https://rest.trikthom.nl/ndi/profile/profile.png',
    alerts: [],
    username: undefined,
    authUserUUID: undefined,
    authUserPass: undefined,
    profile: {},
    RESETgetUser: undefined,
    EVENSTgetEvents: [],
    dialog: false,
    dialogHtml: undefined,
    user: {},
    version: 'NDI v2.0 made by TrikThom',
    isResetPassword: false,
    bestuur: [],
    kader: []
  },



  getters: {
    users(state) {
      return state.users;
    },
    version(state) {
      return state.version;
    },
    username(state) {
      return state.username;
    },
    authUserUUID(state) {
      state.authUserUUID = localStorage.getItem('authUserUUID');
      return state.authUserUUID;
    },
    authUserPass(state) {
      state.authUserPass = localStorage.getItem('authUserPass');
      return state.authUserPass;
    },
    authProfilePhoto(state) {
      return state.authProfilePhoto;
    },
    authUser(state) {
      return state.user;
    },
    alerts(state) {
      return state.alerts;
    },
    getProfile(state) {
      return state.profile;
    },
    RESETgetUser(state) {
      return state.RESETgetUser;
    },
    EVENSTgetEvents(state) {
      return state.EVENSTgetEvents;
    },
    getDialog(state) {
      return state.dialog;
    },
    getDialogHtml(state) {
      return state.dialogHtml;
    },
    bestuur(state) {
      return state.bestuur;
    },
    kader(state) {
      return state.kader;
    }
  },




  mutations: {
    addUser(state, payload) {
      state.users.push(payload.userData);
    },
    getUsers(state, payload) {
      state.users = [];
      for(var i = 0; i < payload.users.length; i++) {
        state.users.push(payload.users[i]);
      }
    },
    logout(state) {
      state.authUserUUID = undefined;
      state.authUserPass = undefined;
      localStorage.removeItem('authUserUUID');
      localStorage.removeItem('authUserPass');
      router.push({name: 'home'})
    },
    authProfilePhoto(state, payload) {
      if(payload.authProfilePhoto != undefined) {
        state.authProfilePhoto = payload.authProfilePhoto;
      }
    },
    login(state, payload) {
      Vue.http.post(state.ip + '/login', payload.userData, {
        emulateJSON: true,
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {
        // Success
        localStorage.setItem("authUserUUID", response.data.public_id);
        state.authUserUUID = localStorage.getItem('authUserUUID');
        localStorage.setItem("authUserPass", response.data.password);
        state.authUserPass = localStorage.getItem('authUserPass');
        router.push({name: 'home'});
      },function (response) {
          // Error
          store.commit({
            type: 'alert',
            message: response.data.error.text,
            alertType: 'error',
            value: true
          });
      });

    },
    alert(state, payload) {
      state.alerts.push({
        message: payload.message,
        type: payload.alertType,
        value: payload.value,
        show: true
      });
      setTimeout(function(){
        state.alerts[0].show = false;
      }, 2950)
      setTimeout(function(){
        state.alerts.splice(0, 1);
      }, 3000)
    },
    saveProfileInfo(state, payload) {
      state.profile = payload.data;
    },
    RESETsetUser(state, payload) {
      state.RESETgetUser = payload.username;
    },
    EVENTSsetEvents(state, payload) {
      state.EVENSTgetEvents = [];
      for(var i = 0; i < payload.events.length; i++) {
        payload.events[i].visible = false;
        state.EVENSTgetEvents.push(payload.events[i]);
      }
    },
    setDialog(state, payload) {
      state.dialog = payload;
    },
    createDialog(state, payload) {
      state.dialog = payload.dialog.dialog;
      state.dialogHtml = payload.dialog
    }
  },




  actions: {
    async check(context, payload) {
      Vue.http.post(context.state.ip + '/login', {
        user: localStorage.getItem('authUserUUID'),
        password: localStorage.getItem('authUserPass')
      }, {
        emulateJSON: true,
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {

      },function (response) {
          // Error
          context.commit({
            type: 'alert',
            message: 'Je wachtwoord is gewijzigd! Log je opnieuw in a.u.b.',
            alertType: 'error',
            value: true
          });
          context.commit('logout');
      });
    },
    addUser(context, payload) {
      context.commit({
        type: 'addUser',
        userData: payload.userData
      });
    },
    getUsers(context, payload) {
      Vue.http.get(context.state.ip + '/users', {
      }).then(
        function(res) {
          context.commit({
            type: 'getUsers',
            users: res.data.users
          });
        },
        function(res) {
          console.log('no users found');
        }
      );



    },
    login(context, payload) {

      context.commit({
        type: 'login',
        userData: payload.userData
      });
      context.dispatch({
        type: 'updateAuthProfilePhoto',
        UUID: payload.userData.user
      });

    },
    updateAuthProfilePhoto(context, payload) {
      context.dispatch({
        type: 'authProfilePhoto',
        user: payload.UUID
      });
    },
    logout(context) {
      context.commit('logout');
    },
    async authProfilePhoto(context, payload) {
      Vue.http.get(context.state.ip + '/users/' + payload.user, {
      }).then(
        function(res) {
          context.commit({
            type: 'authProfilePhoto',
            authProfilePhoto: context.state.ip + '/profile/profile.png'
          });
        },
        function(res) {
          context.commit({
            type: 'authProfilePhoto',
            authProfilePhoto: context.state.ip + '/profile/' + res.data.profilePhoto
          });
          context.state.username = res.data.firstName
          context.state.user = res.data
        }
      );

    },
    async resetPassword(context, payload) {
      if(context.state.isResetPassword) {
        context.commit({
          type: 'alert',
          message: "Je wachtwoord wordt al gereset. Even gedult.",
          alertType: 'warning',
          value: true
        });
      }
      else {
        if(payload.password == payload.spassword) {
          if(payload.password.length < 5) {
            context.commit({
                type: 'alert',
                message: "Je moet minimum 5 karakters hebben!",
                alertType: 'error',
                value: true
            });
          }
          else {
            context.commit({
              type: 'alert',
              message: "Je wachtwoord wordt gereset.",
              alertType: 'info',
              value: true
            });
            context.state.isResetPassword = true;
            Vue.http.post(context.state.ip + '/reset', {
              password: payload.password,
              key: payload.key
            }, {
              emulateJSON: true,
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }).then(
              function(res) {
                context.commit({
                  type: 'alert',
                  message: res.data.success.text,
                  alertType: 'success',
                  value: true
                });
                context.dispatch({
                  type: 'login',
                  userData: {
                    user: payload.user,
                    password: payload.password
                  }
                });
                context.state.isResetPassword = false
              },
              function(res) {
                context.commit({
                  type: 'alert',
                  message: res.data.error.text,
                  alertType: 'error',
                  value: true
                });
                context.state.isResetPassword = false
              }
            );
          }
        }
        else {
          context.commit({
              type: 'alert',
              message: "Je hebt niet twee keer hetzelfde wachtwoord ingegeven!",
              alertType: 'error',
              value: true
            });
        }
      }
    },



    async getProfileInfo(context, payload){
      Vue.http.get(context.state.ip + '/users/' + payload.user, {
      }).then(
        function(res) {
          context.commit({
            type: 'alert',
            message: res.data.error.text,
            alertType: 'error',
            value: true
          });
          if(router.push(-1) == undefined) {
            router.push('/')
          }
          else {
            router.push(-1)
          }
        },
        function(res) {
          context.commit({
            type: 'saveProfileInfo',
            data: res.data
          });
        }
      );
    },

    async bestuur(context, payload){
      Vue.http.get(context.state.ip + '/bestuur', {
      }).then(
        function(res) {
          context.state.bestuur = res.data.bestuur
        },
        function(res) {
          context.commit({
            type: 'alert',
            message: res.data.error.text,
            alertType: 'error',
            value: true
          });
        }
      );
    },
    async kader(context, payload){
      Vue.http.get(context.state.ip + '/kader', {
      }).then(
        function(res) {
          context.state.kader = res.data.kader
        },
        function(res) {
          try{
            context.commit({
              type: 'alert',
              message: res.data.error.text,
              alertType: 'error',
              value: true
            });
          }
          catch(error) {
            context.commit({
              type: 'alert',
              message: "UNKNOWN ERROR: " + error,
              alertType: 'error',
              value: true
            });
          }
        }
      );
    },


    async sendEmail(context, payload) {
      Vue.http.get(context.state.ip + '/users/' + payload.user).then(
        function(res) {
          context.commit({
            type: 'alert',
            message: 'Email niet succesvol verzonden met reden: ' + res.data.error.text,
            alertType: 'error',
            value: true
          });
        },
        function(res) {
          context.commit({
            type: 'alert',
            message: 'Een email wordt verstuurd naar ' + res.data.email,
            alertType: 'info',
            value: true
          });
          Vue.http.post(context.state.ip + '/mail/' + payload.typ, {
            email: res.data.email,
            uuid: res.data.public_id
          }, {
            emulateJSON: true,
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }).then(
            function(res) {
              context.commit({
                type: 'alert',
                message: res.data.success.text,
                alertType: 'success',
                value: true
              });
            },
            function(res) {
              context.commit({
                type: 'alert',
                message: res.data.error.text,
                alertType: 'error',
                value: true
              });
            }
          );
        }
      );
    },



    async RESETgetUser(context, payload) {
      Vue.http.get(context.state.ip + '/reset/' + payload.key).then(
        function(res) {
          context.commit({
            type: 'alert',
            message: res.data.error.text,
            alertType: 'error',
            value: true
          });
        },
        function(res) {
          context.commit({
            type: 'authProfilePhoto',
            authProfilePhoto: context.state.ip + '/profile/' + res.data.profilePhoto
          });
          context.commit({
            type: 'RESETsetUser',
            username: res.data.username
          })
        }
      );
    },

    async EVENSTgetEvents(context, payload) {
      Vue.http.post(context.state.ip + '/events', payload.user, {
        emulateJSON: true,
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {
        // Success
        context.commit({
          type: 'EVENTSsetEvents',
          events: response.data,
        });
      },function (response) {
          // Error
          try{
            context.commit({
              type: 'alert',
              message: response.data.error.text,
              alertType: 'error',
              value: true
            });
          }
          catch(error) {
            context.commit({
              type: 'alert',
              message: "UNKNOWN ERROR: " + error,
              alertType: 'error',
              value: true
            });
          }

      });
    },
    async getDialog(context, payload) {
      Vue.http.post(context.state.ip + '/dialogs', payload.dialog, {
        emulateJSON: true,
        headers: {
          'Content-Type': 'multipart/form-data',
          'Content-Type': 'application/pdf'
        }
      }).then(function (response) {
        // Success
        context.commit({
          type: 'setDialog',
          events: payload.dialog.dialog,
        });
        context.state.dialogHtml = response.data;

      },function (response) {
        // Error
          try{
            context.commit({
              type: 'alert',
              message: response.data.error.text,
              alertType: 'error',
              value: true
            });
          }
          catch(error) {
            context.commit({
              type: 'alert',
              message: "UNKNOWN ERROR: " + error,
              alertType: 'error',
              value: true
            });
          }
      });
    },
    async subscribe(context, payload) {
      context.state.EVENSTgetEvents[payload.id].isIngeschreven = 'loading'
      context.commit({
        type: 'alert',
        message: 'Bezig met inschrijven...',
        alertType: 'info',
        value: true
      });
      Vue.http.post(context.state.ip + '/subscribe', payload.user, {
        emulateJSON: true
      }).then(function (response) {
        // Success
        context.commit({
          type: 'alert',
          message: response.data.success.text,
          alertType: 'success',
          value: true
        });
        context.state.EVENSTgetEvents[payload.id].isIngeschreven = true

      },function (response) {
        // Error
        context.state.EVENSTgetEvents[payload.id].isIngeschreven = false
          try{
            context.commit({
              type: 'alert',
              message: response.data.error.text,
              alertType: 'error',
              value: true
            });
          }
          catch(error) {
            context.commit({
              type: 'alert',
              message: "UNKNOWN ERROR: " + error,
              alertType: 'error',
              value: true
            });
          }
      });
    },
    async createEvent(context, payload) {
      Vue.http.post(context.state.ip + '/event/create', {
        uuid: localStorage.getItem('authUserUUID'),
        password: localStorage.getItem('authUserPass'),
        event: payload.event
      }, {
        emulateJSON: true
      }).then(function (response) {
        // Success
        context.commit({
          type: 'alert',
          message: response.data.success.text,
          alertType: 'success',
          value: true
        });
        context.state.dialog = 'dashboard';
        context.state.dialogHtml.title = 'Dashboard';
      },function (response) {
        // Error
          try{
            context.commit({
              type: 'alert',
              message: response.data.error.text,
              alertType: 'error',
              value: true
            });
          }
          catch(error) {
            context.commit({
              type: 'alert',
              message: "UNKNOWN ERROR: " + error,
              alertType: 'error',
              value: true
            });
          }
      });
    },
    async unsubscribe(context, payload) {
      context.commit({
        type: 'alert',
        message: 'Bezig met uitschrijven...',
        alertType: 'info',
        value: true
      });
      Vue.http.post(context.state.ip + '/unsubscribe', payload.user, {
        emulateJSON: true
      }).then(function (response) {
        // Success
        context.commit({
          type: 'alert',
          message: response.data.success.text,
          alertType: 'success',
          value: true
        });
        context.state.EVENSTgetEvents[payload.id].isIngeschreven = false

      },function (response) {
        // Error
          try{
            context.commit({
              type: 'alert',
              message: response.data.error.text,
              alertType: 'error',
              value: true
            });
          }
          catch(error) {
            context.commit({
              type: 'alert',
              message: "UNKNOWN ERROR: " + error,
              alertType: 'error',
              value: true
            });
          }
      });
    },
    setDialog(context, payload) {
      context.dispatch({
        type: 'getDialog',
        dialog: {
          dialog: payload.dialog
        }
      })
    },



  }
});
